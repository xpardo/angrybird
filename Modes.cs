using System;


class Modes
{
    private int mode;
    private void SetMode(int option){
        this.mode = option; ///<value>Estableix el mode como la opció introduïda</value>
    }
    public int GetMode(){
        return mode; ///<value>Retorna el mode elegit</value>
    }

    public void ModeMenu(){
        int option = 0;
        string op;
        //while en que se pregunta cantos pájaros quieres
        do{
            Console.WriteLine("\nElige un mode");
            Console.WriteLine("\n 1 Mode 1 Angry Bird");
            Console.WriteLine("\n 3 Mode 3 Angry Bird");
            Console.WriteLine("\n 5 Mode 5 Angry Bird");
            Console.WriteLine("\nEscriu el numero de Angry Birds que bulguis(1,3,5).");
            op = Console.ReadLine();
            try{
                option = Convert.ToInt32(op);
            } catch (System.FormatException) {
              
                ///<value> Si el caracter introduït no es numerp escriu el error i el torna preguntar</value>
                Console.WriteLine("\nEl valor introduït no es un numero!");
                Console.ReadLine();
                continue;
            }
             ///<value> Comprova que el caracte introduït en una de les opcios disponibles</value>
       
        }while(option != 1 && option != 3 && option != 5);
            ///<value> Crdia a la funcio o li pasa el mode elegit</value>
        SetMode(option); 
    }



}

