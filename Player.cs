using System;


/// <summary>
/// clase Player
///Aquesta classe crea la baralla de cartas, el jugador 
/// </summary>
class Player{
        
    private int puntuacio;
    private Carta cart;
    private string[] usarCarta;



    /// <value>Crear el nou jugador amb els seus puts i baralla</value>
    public Player(Carta cart,Modes mode){
        this.puntuacio=0;
        this.cart=cart;
        this.usarCarta= new string[mode.GetMode()];
    }

    ///<value>Busca en la baralla  la carta que el usuari utilitza en cada torn </value>
    public void SelectCard(int AngryBird,int index){  
    
        AngryBird[] tmpCarta = this.cart.GetCarta();
        usarCarta[index] = tmpCarta[(AngryBird-1)].GetNom();
    }
    
    ///<value>mostra els usells disponibles</values>  
    public void ShowAvailableCarta(){
        AngryBird[] tmpCarta = this.cart.GetCarta();      
        int info = 1;
        for(int i = 0;i<tmpCarta.Length;i++){
            int index = Array.IndexOf(this.usarCarta, tmpCarta[i].GetNom());
            if(index == -1){
                Console.WriteLine(info+tmpCarta[i].showAngry());
            }
            info +=1;
        }
    }

    ///<value>Suma un punt al jugado que a guanyat la ronda</value>
        public void SetPuntuacio(){     
        this.puntuacio+=1;
    }
        ///<value>Retorna tota la barrala del jugador</value>
    public Carta GetPlayerCarta(){    
        return this.cart;
    }  
    public Carta GetPlayer2Carta(){    
        return this.cart;
    }  
            ///<value>Retorna el usercarta del sugador</value>
    public string[] GetUsedCarta(){      
        return this.usarCarta;
    } 
        ///<value>Retorna els punts dels usells selecionats per despres comparar</value>
    public int GetPoints(){     
        return this.puntuacio;
    }
   
        
}

