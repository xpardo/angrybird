﻿using System;


/// <summary>
/// clase principal AngryBird.
/// Conte tots els valors de la AngryBird.
/// </summary>
class AngryBird{
    public string nom;
    public string especie;
    public string poder;
    public int energia;

    public AngryBird(string nom, string especie, string poder, int energia){
        this.nom=nom;
        this.especie=especie;
        this.poder=poder;
        this.energia=energia;
    }

    /// <value>Aques metode torna el atribut nom</value>
    public string GetNom(){
        return nom;
    }
    /// <value>Aques metode torna el atribut especie</value>
    public string GetEspecie(){
        return especie;
    }
    /// <value>Aques metode torna el atribut poder</value>
    public string GetPoder(){
        return poder;
    }
    /// <value>Aques metode torna el atribut energia</value>
    public int GetEnergia(){
        return energia;
    }
    /// <value>mostra totes les dadaes del ocell</value>
    public string showAngry(){
        string value = "."+nom+" / "+especie+" / "+poder+" / "+energia;
        return value;
    }
}
       
    
    


