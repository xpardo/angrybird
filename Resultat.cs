using System;


class Resultat
{

    public void Winner(Player jugador1, Player jugador2){
        ///<value>Comprova quina es el usell amb més energia i mostra qui es el vencedor de torn</value>
         
        if (jugador1.GetPoints() > jugador2.GetPoints()){ ///<value>Guanya el Jugador1</value>
            Console.WriteLine($"\nEl Guanyador es el Jugador1 {jugador1.GetPoints()}-{jugador2.GetPoints()}");
        }else if (jugador1.GetPoints() < jugador2.GetPoints()){  ///<value>Guanya el Jugador2</value>
            Console.WriteLine($"\nEl Guanyador es el jugador2 {jugador2.GetPoints()}-{jugador1.GetPoints()}");
        }else {  ///<value>Es un empate</value>
            Console.WriteLine($"\n Es un empate Jugador1:{jugador1.GetPoints()} / jugador2:{jugador2.GetPoints()}");
        }
        Console.ReadLine();

    }
}

