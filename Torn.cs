using System;


/// <summary>
///Aquesta es on succeeix la majoria del programa
/// </summary>

class Torn
{

    ///<value>Aquest atribut s'utilitsara per comptar el numero de turns que sucedeix</value>
    private int turnCount;

    ///<value>Aquest metode s'utilitza per iniciar els turns</value>
    public Torn(int contador){ 
        this.turnCount = contador;
    }


    ///<value>calcula quin dels jugadors es el guanyador comparant la variable energy de cada jugador</value>

    private void calculateGuanyadorTorn(Player jugador1, Player jugador2,int selectedBird){ 

        ///<value>aqui resivim els atributs que necesitem desde els parametres pque es  pasan per el metode</value>
        AngryBird[] tmpPlayerDeck = (jugador1.GetPlayerCarta()).GetCarta(); 
        int playerEnergy = tmpPlayerDeck[(selectedBird-1)].GetEnergia();
        AngryBird[] tmpjugador2Deck = (jugador2.GetPlayer2Carta()).GetCarta();
        int jugador2Energy = tmpjugador2Deck[(selectedBird-1)].GetEnergia();
        Console.WriteLine($"\n{tmpPlayerDeck[(selectedBird-1)].GetNom()} vs {tmpjugador2Deck[(selectedBird-1)].GetNom()}!");
        if(playerEnergy > jugador2Energy){
            jugador1.SetPuntuacio();
            Console.WriteLine($"El teu usell  {tmpPlayerDeck[(selectedBird-1)].GetNom()} Guanya amb {playerEnergy} de energia al seu usell  {tmpjugador2Deck[(selectedBird-1)].GetNom()} amb {jugador2Energy} de energia!");
        } else if(playerEnergy < jugador2Energy){
            jugador2.SetPuntuacio();
            Console.WriteLine($"el teu usell {tmpjugador2Deck[(selectedBird-1)].GetNom()} Guanya amb {jugador2Energy} de energia al teu pajaro {tmpPlayerDeck[(selectedBird-1)].GetNom()} amb {playerEnergy} de energia!");
        } else {
            Console.WriteLine($"els dos usells tenen {jugador2Energy} energia, es empate!");
        }

    }

   
    ///<value>Aquest metode es on sucedeix la eleció dels ocells del joc</value>
    public void Turns(Player jugador1, Player jugador2,Modes mode){  
        int turns = mode.GetMode();

        ///<value>whilw es on succedeix tot mentre que el contdor de turn no sigui igual a turns, que contenten el maxim de turns degut al mode agafat anteriorment a la clase Joc</value>
        while(turnCount < turns){ 
            int tmpSelect;
            string tmp;
            AngryBird[] tmpPlayerCarta;
            bool tmpBool = false;
            bool missClick = false;

            ///<value>Aquest do while comprovara vaires coses(el usell seleccionat estigui a la baralla, l'osell seleccionat no s'hagi fet servir ja ..)sobre l'elecció abans de passar el valor introduït als mètodes del programa</value>
            do{ 
                if(missClick){
                    Console.WriteLine("Selecciona un de Aquest usells  escribin el su numero");
                    missClick = false;
                } else{
                    Console.WriteLine("\nSelecciona un de Aquest usells  escribin el su numero");

                }

                ///<value>jugador 1</value>
                jugador1.ShowAvailableCarta(); 
                tmp = Console.ReadLine();
               
                try{
                    tmpSelect = Convert.ToInt32(tmp);
                } catch (System.FormatException) {
                    Console.WriteLine("\nEl valor introduït no es un numero !");
                    continue;
                }

                tmpPlayerCarta = (jugador1.GetPlayerCarta()).GetCarta();
                if(tmpSelect <= turns && tmpSelect >= 1){
                    if((Array.IndexOf(jugador1.GetUsedCarta(), tmpPlayerCarta[(tmpSelect-1)].GetNom()) == -1)){
                        tmpBool = true;
                    } else {
                        Console.WriteLine("\n Aquell usell ja a sigut agafat anteriorment!");
                        missClick = true;
                    }
                } else {
                    Console.WriteLine("\n Te que ser un dels numeros mostrats !");
                    missClick = true;
                }


                ///<value>jugador 2</value>
                jugador2.ShowAvailableCarta(); 
                tmp = Console.ReadLine();

                 try{
                    tmpSelect = Convert.ToInt32(tmp);
                } catch (System.FormatException) {
                    Console.WriteLine("\nEl valor introduït no es un numero !");
                    continue;
                }

                tmpPlayerCarta = (jugador2.GetPlayer2Carta()).GetCarta();
                if(tmpSelect <= turns && tmpSelect >= 1){
                    if((Array.IndexOf(jugador2.GetUsedCarta(), tmpPlayerCarta[(tmpSelect-1)].GetNom()) == -1)){
                        tmpBool = true;
                    } else {
                        Console.WriteLine("\n Aquell usell ja a sigut agafat anteriorment!");
                        missClick = true;
                    }
                } else {
                    Console.WriteLine("\n Te que ser un dels numeros mostrats !");
                    missClick = true;
                }



                ///<value>Quan se surt del do while i s'ha escollit un ocell vàlid, el valor serà passat als mètodes necessaris. </value>
            }while(!tmpBool); 
                tmpSelect = Convert.ToInt32(tmp);
               
                jugador2.SelectCard(tmpSelect,turnCount);
                jugador1.SelectCard(tmpSelect,turnCount);
                calculateGuanyadorTorn(jugador1,jugador2,tmpSelect);
                Console.ReadLine();
                turnCount+=1;
        }
        ///<value>Per ultim al final de aquest metode es dirigira a la clase Result on es calculara el resultat del joc</value>
        Resultat end = new Resultat(); 
        end.Winner(jugador1,jugador2);
    }



}
