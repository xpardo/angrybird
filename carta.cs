using System;


/// <summary>
///  guarda una baralla del usells per jugador
/// </summary>
        
class Carta{
    /// <value>Aquest array conter els ocells que conte el jugador</value>
    private AngryBird[] cart;

    public void SetRandomCarta(AngryBird[] angry, Modes mode){
        cart = new AngryBird[mode.GetMode()]; 
        var rando = new Random(); 
        int cnt = 0;
        int AngryIndex;
        bool tmp;

        while(cnt != cart.Length){ ///<value>Aqui se generan els usells aleatoriament i fem que no poden repetir la mateixa baralla /<value>
            do{
                AngryIndex = rando.Next(0, 9);
                tmp = true;
                int index = Array.IndexOf(cart, angry[AngryIndex]);
                if(index == -1){
                    tmp = false;
                    break;
                }
            }while(tmp);
            cart[cnt] = angry[AngryIndex];
            cnt+=1;
        }

    }

    ///<value>Aquest metode torna el atribut carta de aquesta clase</value>
    public AngryBird[] GetCarta(){
        return cart;
    }


}