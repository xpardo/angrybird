using System;


/// <summary>
/// clase programa
///Aquesta classe és la classe d'on cauen la resta
/// </summary>

class Program
{

    /// <value>primer crear tots el objectes tipus AngryBird necesaris per el joc</value>
   
    public static void  Start(){

        AngryBird rojo          = new AngryBird("Red","cardenal","ninguno",2);
        AngryBird amarillo      = new AngryBird("Chuck","canario","velocidad",23);
        AngryBird azul          = new AngryBird("Jay,Jake y Jim","azulejo","dividirse en tres",64);
        AngryBird verde         = new AngryBird("Hal","tucán","bumerang",45);
        AngryBird negro         = new AngryBird("Bomb","cuervo","explota",67);
        AngryBird blanco        = new AngryBird("Matilda","gallina","lanza un huevo explosivo",91);
        AngryBird naranja       = new AngryBird("Bubbles","gorrión","hincharse",13);
        AngryBird rosa          = new AngryBird("Stella","cacatua Galah","hacer burbujas",31);
        AngryBird rojoGordo     = new AngryBird("Terence","cardenal","su peso",44);
        AngryBird [] angryBirds={rojo,amarillo,azul,verde,negro,blanco,naranja,rosa,rojoGordo};

        ///<value> Aquí as crea l'objecte classe welcome perquè salti l'explicació </value>
        Welcome explicar = new Welcome();
        explicar.explicar();
        ///<value> es crear l'objecte classe joc ques s'utilitzarà per generar les baralles, els jugadors i altres objectes necessaris</value>
        Joc play = new Joc();
        string opcio;

        do{
            Menu();
            opcio = Console.ReadLine();
            switch (opcio){
                case "p":
                    play.Iniciar(angryBirds);
                break;
            }
        }while(opcio!="ex");
            
            
    }
     public static void Menu(){
        Console.WriteLine("\nVols jugar?");
        Console.WriteLine("\n p. PLAY");
        Console.WriteLine("\n ex. EXIT");
    }


       
}




