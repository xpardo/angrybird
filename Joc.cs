using System;

/// <summary>
/// s'utilitzarà per crear i generar alguns objectes importants
/// <summary>
class Joc{
    ///<value>Aquest mètode connecta les classes i crea objectes importants com el mode, les baralles, els jugadors i torn</value>
    public void Iniciar(AngryBird [] angryBirds ){
        Modes mode = new Modes();
        mode.ModeMenu();

        
        Carta playerCarta = new Carta();
        playerCarta.SetRandomCarta(angryBirds,mode);
        

        Carta player2Carta = new Carta();
        player2Carta.SetRandomCarta(angryBirds,mode);

        Player jugador1 = new Player(playerCarta,mode);
        Player jugador2 = new Player(player2Carta,mode);

        Torn partida = new Torn(0);
        partida.Turns(jugador1,jugador2,mode);

    }
    
    
    
}
